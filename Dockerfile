FROM python:3.8
WORKDIR /code
COPY requirements.txt .
RUN pip install flask && pip install datetime
COPY src/ .
CMD [ "python", "./server.py" ]
