from flask import Flask
from datetime import datetime
server = Flask(__name__)

@server.route("/")

def hello():

    return "Hello World!"

if __name__ == "__main__":
   now = datetime.now()
   current_time = now.strftime("%H:%M:%S")
   print("Current Time =", current_time)

   server.run(host='0.0.0.0')
